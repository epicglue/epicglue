package main

import (
	"gitlab.com/epicglue/epicglue/app/api/api"
)

func main() {
	api.Run()
}
