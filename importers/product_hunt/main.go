package main

import "gitlab.com/epicglue/product-hunt-cli/cmd"

func main() {
	cmd.Execute()
}
