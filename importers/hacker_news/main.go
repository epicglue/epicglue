package main

import "gitlab.com/epicglue/epicglue/importers/hacker_news/cmd"

func main() {
	cmd.Execute()
}
